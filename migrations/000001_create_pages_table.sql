-- +migrate Up
CREATE TABLE IF NOT EXISTS pages (
  page_id serial PRIMARY KEY,
  version int NOT NULL,
  url VARCHAR(50) NOT NULL,
  text_path VARCHAR(50) NOT NULL,
  raw_text_path VARCHAR(50) NOT NULL
);

-- +migrate Down
DROP TABLE IF EXISTS pages;
