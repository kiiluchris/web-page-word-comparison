package word_comparison

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
)

type WordCountServer struct {
	wordCountApp *WordCountApp
	pageStore    PageStore
	http.Handler
}

const HeaderContentType = "Content-Type"
const HeaderAccept = "Accept"
const ContentTypeJson = "application/json"
const ContentTypeHtml = "text/html"

var ErrBadRequest = errors.New("Bad request given")

func NewHtmlWordCountServer(
  fileStorageDir string,
  postgresHost string,
  postgresDatabase string,
  postgresUser string,
  postgresPassword string,
) *WordCountServer {
	fetcher := NewHttpPageFetcher()
	parser := NewHtmlParser(true, true)
	fileStore := NewFileSystemFileStore(fileStorageDir)
	app := NewWordCountApp(fetcher, parser, fileStore)
	pageStore := NewPostgresPageStore(
		postgresHost,
		postgresDatabase,
		postgresUser,
		postgresPassword,
		fileStore,
	)
  return NewWordCountServer(app, pageStore)
}

func NewWordCountServer(wca *WordCountApp, pageStore PageStore) *WordCountServer {
	server := new(WordCountServer)
	server.wordCountApp = wca
	server.pageStore = pageStore
	router := http.NewServeMux()
	router.HandleFunc("/api/word-counts-fetch", server.CountWordsWithFetch)
	router.HandleFunc("/api/word-counts", server.CountWords)
	router.HandleFunc("/api/page/compare", server.PageComparison)
	router.HandleFunc("/api/page", server.PagesHandler)
	server.Handler = router

	return server
}

func SetContentTypeJson(w http.ResponseWriter) {
	w.Header().Set(HeaderContentType, ContentTypeJson)
}

func SetContentTypeHtml(w http.ResponseWriter) {
	w.Header().Set(HeaderContentType, ContentTypeHtml)
}

func HandleFileFetchError(w http.ResponseWriter, err error) {
	switch err {
	case ErrFileDoesNotExist:
		w.WriteHeader(http.StatusNotFound)
	case ErrFileIsNotHtml:
		w.WriteHeader(http.StatusUnsupportedMediaType)
	case ErrBadRequest:
		w.WriteHeader(http.StatusBadRequest)
	}
	w.Write([]byte(err.Error()))
}

func (s *WordCountServer) CountWords(w http.ResponseWriter, r *http.Request) {
	SetContentTypeJson(w)
	id := r.URL.Query().Get("id")
	if id == "" {
		w.Write([]byte("[]"))
		return
	}
	idInt, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("id can only be an integer"))
		return
	}
	text, err := s.pageStore.GetPageProcessedTextById(idInt)

	if err != nil {
		HandleFileFetchError(w, err)
	} else {
		wcl := WordCounts(text).ToList()
		json.NewEncoder(w).Encode(wcl)
	}
}

func (s *WordCountServer) CountWordsWithFetch(w http.ResponseWriter, r *http.Request) {
	SetContentTypeJson(w)
	url := r.URL.Query().Get("url")
	if url != "" {
		wcm, err := s.wordCountApp.Execute(url)
		if err != nil {
			HandleFileFetchError(w, err)
		} else {
			json.NewEncoder(w).Encode(wcm)
		}
	} else {
		w.Write([]byte("{}"))
	}
}

func (s *WordCountServer) PagesHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		s.ListPages(w, r)
	case http.MethodPost:
		s.CreatePage(w, r)
	}
}

type pagesListPage struct {
	Pages []Page
}

func (s *WordCountServer) ListPages(w http.ResponseWriter, r *http.Request) {
	pages, err := s.pageStore.GetPages()
	if err != nil {
		HandleFileFetchError(w, err)
	} else {
		SetContentTypeJson(w)
		json.NewEncoder(w).Encode(pages)
	}
}

type pageCreateRequest struct {
	Url string `json:"url"`
}

type PageFetchResponseData struct {
	Id  int    `json:"id"`
	Url string `json:"url"`
}

type PageFetchResponse struct {
	Message string                `json:"message"`
	Data    PageFetchResponseData `json:"data"`
}

const MessageSuccess = "success"

func (s *WordCountServer) CreatePage(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get(HeaderContentType) != ContentTypeJson {
		w.WriteHeader(http.StatusUnsupportedMediaType)
		w.Write([]byte("Only json data supported"))
		return
	}
	var data pageCreateRequest
	err := json.NewDecoder(r.Body).Decode(&data)
	if data.Url == "" {
		HandleFileFetchError(w, ErrBadRequest)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Incorrectly formed json data given"))
		return
	}
	fetchedData, err := s.wordCountApp.FetchAndParse(data.Url)
	if err != nil {
		HandleFileFetchError(w, err)
		return
	}
	SetContentTypeJson(w)
	page, _ := s.pageStore.AddPage(data.Url, fetchedData.FetchedPage, fetchedData.ProcessedPage)
	resData := PageFetchResponse{
		Message: MessageSuccess,
		Data: PageFetchResponseData{
			Id:  page.Id,
			Url: page.Url,
		},
	}
	json.NewEncoder(w).Encode(resData)
}

func mapStringListToIntList(strings []string) []int {
	numbers := make([]int, 0, len(strings))
	for _, text := range strings {
		number, err := strconv.Atoi(text)
		if err == nil {
			numbers = append(numbers, number)
		}
	}
	return numbers
}

func (s *WordCountServer) PageComparison(w http.ResponseWriter, r *http.Request) {
	SetContentTypeJson(w)
	idStringList := strings.Split(r.URL.Query().Get("pages"), ",")
	ids := mapStringListToIntList(idStringList)
	var wordCountComparisonData WordCountComparisonData
	if len(ids) == 0 {
		wordCountComparisonData = WordCountComparisonData{}
	} else {
		wordCountMaps := make([]WordCountMapWithPage, 0, len(ids))
    pages, _ := s.pageStore.GetPagesByIds(ids)
		for _, page := range pages {
			text, err := s.pageStore.GetPageProcessedTextById(page.Id)
			if err != nil {
				continue
			}
			wcm := WordCountMapWithPage{
				Page:         page,
				WordCountMap: WordCounts(text),
			}
			wordCountMaps = append(wordCountMaps, wcm)
		}
		wordCountComparisonData = CompareWordCountMaps(wordCountMaps)
	}
	json.NewEncoder(w).Encode(&wordCountComparisonData)
}
