package word_comparison

import (
	"os"
	"path"
)

type FileStore interface {
	SavePage(siteDirectory, rawTextPath, rawText, processedTextPath, processedText string)
	GetFile(path string) (string, error)
	Root() string
}

type FileSystemFileStore struct {
	rootDirectory string
}

func NewFileSystemFileStore(directory string) *FileSystemFileStore {
	if err := os.MkdirAll(directory, DirectoryPerm); err != nil {
		panic(err)
	}

	return &FileSystemFileStore{
		rootDirectory: directory,
	}
}

func (s *FileSystemFileStore) Root() string {
	return s.rootDirectory
}

func (s *FileSystemFileStore) SavePage(siteDirectory, rawTextPath, rawText, processedTextPath, processedText string) {
	siteDirectory = path.Join(s.rootDirectory, siteDirectory)
	rawTextPath = path.Join(s.rootDirectory, rawTextPath)
	processedTextPath = path.Join(s.rootDirectory, processedTextPath)
	os.MkdirAll(siteDirectory, DirectoryPerm)
	os.WriteFile(rawTextPath, []byte(rawText), FileUserRWPerm)
	os.WriteFile(processedTextPath, []byte(processedText), FileUserRWPerm)
}

func (s *FileSystemFileStore) GetFile(filePath string) (string, error) {
	file, err := os.ReadFile(path.Join(s.rootDirectory, filePath))

	if err != nil {
		return "", ErrFileDoesNotExist
	}
	return string(file), nil
}
