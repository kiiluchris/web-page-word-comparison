module gitlab.com/kiiluchris/web-page-word-comparison

go 1.16

require (
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.4
	github.com/rubenv/sql-migrate v1.0.0 // indirect
	golang.org/x/net v0.0.0-20220114011407-0dd24b26b47d
)
