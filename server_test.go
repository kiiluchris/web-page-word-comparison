package word_comparison_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/kiiluchris/web-page-word-comparison"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"
)

func TestWordCountServer(t *testing.T) {
	validHtmlPath := "test.html"
	htmlAtPathThatDoesNotExist := "non-existent-file.html"
	nonHtmlPath := "test.md"

	knownCounts := wordCountList{
		{"cat", 2},
		{"dog", 2},
		{"log", 1},
		{"bat", 1},
		{"asd", 1},
	}

	fetcher := word_comparison.NewFilePageFetcher(testFetchedFileDirectory)
	parser := word_comparison.NewHtmlParser(true, true)
	fileStore := word_comparison.NewFileSystemFileStore(testFileStorageDirectory)
	wca := word_comparison.NewWordCountApp(fetcher, parser, fileStore)
	pageStore := word_comparison.NewPostgresPageStore(
    os.Getenv("POSTGRES_HOST"),
		os.Getenv("POSTGRES_DB"),
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		fileStore,
	)
	server := word_comparison.NewWordCountServer(wca, pageStore)
	defer pageStore.DeleteAllPages()

	t.Run("fetch html", func(t *testing.T) {
		req := newWordCountWithFetchRequest(validHtmlPath)
		res := httptest.NewRecorder()
		server.ServeHTTP(res, req)
		assertHttpStatus(t, http.StatusOK, res.Code)
		wcm := parseWordCountFromResponse(t, res.Body)
		assertWordCounts(t, knownCounts, wcm)
	})
	t.Run("fetch html at path that does not exist", func(t *testing.T) {
		req := newWordCountWithFetchRequest(htmlAtPathThatDoesNotExist)
		res := httptest.NewRecorder()
		server.ServeHTTP(res, req)
		assertHttpStatus(t, http.StatusNotFound, res.Code)
	})
	t.Run("fetch markdown", func(t *testing.T) {
		req := newWordCountWithFetchRequest(nonHtmlPath)
		res := httptest.NewRecorder()
		server.ServeHTTP(res, req)
		assertHttpStatus(t, http.StatusUnsupportedMediaType, res.Code)
	})
	t.Run("list pages", func(t *testing.T) {
		req := newPageListRequest()
		res := httptest.NewRecorder()
		server.ServeHTTP(res, req)
		assertHttpStatus(t, http.StatusOK, res.Code)
		pages := parsePageList(t, res.Body)
		assertPageListEq(t, []word_comparison.Page{}, pages)
	})
	t.Run("create new page", func(t *testing.T) {
		assertFetchNewPageRequest(t, server)
		assertFetchPageListRequest(t, server, 1)
		assertFetchNewPageRequest(t, server)
		assertFetchPageListRequest(t, server, 2)
		req := newWordCountRequest(1)
		res := httptest.NewRecorder()
		server.ServeHTTP(res, req)
		assertHttpStatus(t, http.StatusOK, res.Code)
		wcl := parseWordCountList(t, res.Body)
		expectedWordCounts := word_comparison.WordCountList{
			{"cat", 2},
			{"dog", 2},
			{"asd", 1},
			{"bat", 1},
			{"log", 1},
		}
		if !reflect.DeepEqual(wcl, expectedWordCounts) {
			t.Fatalf("Expected word counts to be %v, but got %v", expectedWordCounts, wcl)
		}
	})
	t.Run("compare two pages same content", func(t *testing.T) {
		assertFetchPageListRequest(t, server, 2)
		req := newPageComparisonRequest("1,2")
		res := httptest.NewRecorder()
		server.ServeHTTP(res, req)
		assertHttpStatus(t, http.StatusOK, res.Code)
		comparisonData := parseWordCountComparisonData(t, res.Body)
		assertWordCountComparisonData(t, comparisonData, 5, []struct {
			key   int
			count int
		}{{1, 0}, {2, 0}})
	})
}

func assertFetchNewPageRequest(t testing.TB, server *word_comparison.WordCountServer) {
	req := newPageFetchRequest(validHtmlPath)
	res := httptest.NewRecorder()
	server.ServeHTTP(res, req)
	assertHttpStatus(t, http.StatusOK, res.Code)
	parsedRes := parsePageFetchResponse(t, res.Body)
	if parsedRes.Message != word_comparison.MessageSuccess {
		t.Fatalf("Unsucessful request")
	}
}

func assertFetchPageListRequest(t testing.TB, server *word_comparison.WordCountServer, expectedLen int) {
	req := newPageListRequest()
	res := httptest.NewRecorder()
	server.ServeHTTP(res, req)
	assertHttpStatus(t, http.StatusOK, res.Code)
	pages := parsePageList(t, res.Body)
	if len(pages) != expectedLen {
		t.Fatal("Expected one page exists found none")
	}
}

func parseWordCountComparisonData(t testing.TB, body io.Reader) (wcl word_comparison.WordCountComparisonData) {
	t.Helper()
	err := json.NewDecoder(body).Decode(&wcl)
	if err != nil {
		t.Fatalf("Unable to parse response from server %q into WordCountsComparisonData: %q", body, err)
	}
	return wcl
}

func parseWordCountList(t testing.TB, body io.Reader) (wcl word_comparison.WordCountList) {
	t.Helper()
	err := json.NewDecoder(body).Decode(&wcl)
	if err != nil {
		t.Fatalf("Unable to parse response from server %q into list of WordCounts: %q", body, err)
	}
	return wcl
}

func parsePageFetchResponse(t testing.TB, body io.Reader) (page word_comparison.PageFetchResponse) {
	t.Helper()
	err := json.NewDecoder(body).Decode(&page)
	if err != nil {
		t.Fatalf("Unable to parse response from server %q into list of pages: %q", body, err)
	}
	return page
}

func parsePageList(t testing.TB, body io.Reader) (pages []word_comparison.Page) {
	t.Helper()
	err := json.NewDecoder(body).Decode(&pages)
	if err != nil {
		t.Fatalf("Unable to parse response from server %q into list of pages: %q", body, err)
	}
	return pages
}

func parsePage(t testing.TB, body io.Reader) (page word_comparison.Page) {
	t.Helper()
	err := json.NewDecoder(body).Decode(&page)
	if err != nil {
		t.Fatalf("Unable to parse response from server %q into map of word counts: %q", body, err)
	}
	return page

}

func parseWordCountFromResponse(t testing.TB, body io.Reader) map[string]uint {
	t.Helper()
	wcm := make(map[string]uint)
	err := json.NewDecoder(body).Decode(&wcm)
	if err != nil {
		t.Fatalf("Unable to parse response from server %q into map of word counts: %q", body, err)
	}
	return wcm
}

func assertHttpStatus(t testing.TB, expected, got int) {
	t.Helper()
	if got != expected {
		t.Fatalf("Expected http status %d but got %d", expected, got)
	}
}

func newWordCountRequest(id int) *http.Request {
	request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/api/word-counts?id=%d", id), nil)
	return request
}

func newWordCountWithFetchRequest(path string) *http.Request {
	request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/api/word-counts-fetch?url=%s", path), nil)
	return request
}

func newPageListRequest() *http.Request {
	request, _ := http.NewRequest(http.MethodGet, "/api/page", nil)
	request.Header.Add("Content-Type", word_comparison.ContentTypeJson)
	return request
}

func newPageDetailRequest(uid string) *http.Request {
	request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/api/page/%s", uid), nil)
	return request
}

func newPageComparisonRequest(ids string) *http.Request {
	request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/api/page/compare?pages=%s", ids), nil)
	return request
}

func newPageFetchRequest(path string) *http.Request {
	body, _ := json.Marshal(map[string]string{
		"url": path,
	})
	reqBody := bytes.NewBuffer(body)
	request, _ := http.NewRequest(http.MethodPost, "/api/page", reqBody)
	request.Header.Set(word_comparison.HeaderContentType, word_comparison.ContentTypeJson)
	return request
}
