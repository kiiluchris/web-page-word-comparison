package word_comparison

import (
	"io/ioutil"
	"net/http"
)

type HttpPageFetcher struct{}

func NewHttpPageFetcher() *HttpPageFetcher {
	return &HttpPageFetcher{}
}

func (f *HttpPageFetcher) FetchPage(path string) (string, error) {
	res, err := http.Get(path)
	if err != nil {
		return "", err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	if http.DetectContentType(body) != HtmlContentType {
		return "", ErrFileIsNotHtml
	}
	return string(body), nil
}
