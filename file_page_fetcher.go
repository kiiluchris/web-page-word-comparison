package word_comparison

import (
	"net/http"
	"os"
	"path"
)

type FilePageFetcher struct {
	rootDirectory string
}

func NewFilePageFetcher(rootDirectory string) *FilePageFetcher {
	return &FilePageFetcher{
		rootDirectory: rootDirectory,
	}
}

const HtmlContentType = "text/html; charset=utf-8"

func (f *FilePageFetcher) FetchPage(filePath string) (string, error) {
	data, err := os.ReadFile(path.Join(f.rootDirectory, filePath))
	if err != nil {
		return "", ErrFileDoesNotExist
	}
	contentType := http.DetectContentType(data)
	if contentType != HtmlContentType {
		return "", ErrFileIsNotHtml
	}
	return string(data), nil
}
