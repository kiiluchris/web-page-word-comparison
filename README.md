# Web Page Word Comparison


A JSON API released as a go http handler to be embedded in a
go web server.
The API fetches an caches requested web pages as files on a local
file system while saving file location in a PostgreSQL database.
The number of unique occurrences of words in the
web pages can then be requested for a single page as well
as in comparison to other pages based on words unique to all
pages and words common among all pages.

The API specification is available at [openapi.yml](./openapi.yaml).


## Requirements

### PostgreSQL

Used to store cached web page metadata.

1. Create a PostgreSQL database that is set up to use SSL connections.

### Go

Version 1.16+ is necessary due to use of the embed feature
to store the database migrations as part of the library.

## Installation

To use the package as a go library it can be installed using

```bash
go get -u gitlab.com/kiiluchris/web-page-word-comparison
```

## Usage


Import the API handler into your application

```go
import "gitlab.com/kiiluchris/web-page-word-comparison"

fn main() {
  wordCountServer := word_comparison.NewHtmlWordCountServer(
     "/path/to/cache/files",
     "$POSTGRES_HOST",
     "$POSTGRES_DB",
     "$POSTGRES_USER",
     "$POSTGRES_PASSWORD",
  )

  // Now add the handler to a router
  // For example
  router := http.NewServeMux()
  router.Handle("/api/", wordCountServer.Handler)
  // Add the rest of your routes
}
```




