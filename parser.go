package word_comparison

import (
	"errors"
	"fmt"
	"io"
	"regexp"
	"strings"

	"golang.org/x/net/html"
)

type Parser interface {
	ParseText(text string) (AST, error)
}

type HtmlParser struct {
	ignoreStyle  bool
	ignoreScript bool
}

func NewHtmlParser(ignoreScript, ignoreStyle bool) *HtmlParser {
	return &HtmlParser{
		ignoreScript: ignoreScript,
		ignoreStyle:  ignoreStyle,
	}
}

type HtmlNode struct {
	tag         string
	text        string
	children    []HtmlNode
}

func (n *HtmlNode) _ExtractText(writer io.Writer) {
	for _, child := range n.children {
		if child.tag == textTag {
			for _, word := range strings.Fields(strings.ToLower(child.text)) {
					fmt.Fprintf(writer, "%s ", word)
			}
		} else {
			child._ExtractText(writer)
		}
	}

}

func (n *HtmlNode) ExtractText() string {
	var sb strings.Builder
	n._ExtractText(&sb)
	return sb.String()
}

var ErrInvalidHtml = errors.New("Invalid HTML given, could not fully parse it")

const textTag = "text-node"
const scriptTag = "script"
const styleTag = "style"
const bodyTag = "body"

func (p *HtmlParser) ignoreScriptNode(n *html.Node) bool {
	return n.Data == scriptTag && p.ignoreScript
}

func (p *HtmlParser) ignoreStyleNode(n *html.Node) bool {
	return n.Data == styleTag && p.ignoreStyle
}

func (p *HtmlParser) ParseText(text string) (AST, error) {
	doc, parseErr := html.Parse(strings.NewReader(text))
	if parseErr != nil {
		return nil, ErrInvalidHtml
	}
	reg, _ := regexp.Compile("[^a-zA-Z\\s]+")
	var extractHtmlNodes func(*html.Node) *HtmlNode
	extractHtmlNodes = func(n *html.Node) *HtmlNode {
		if n == nil || (n.Type == html.ElementNode && (p.ignoreStyleNode(n) || p.ignoreScriptNode(n))) {
			return nil
		}
		node := &HtmlNode{
			tag: n.Data,
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			switch c.Type {
			case html.ElementNode:
				child := extractHtmlNodes(c)
				if child != nil {
					node.children = append(node.children, *child)
				}
			case html.TextNode:
				child := HtmlNode{
					tag:         textTag,
					text: reg.ReplaceAllString(c.Data, ""),
				}
				node.children = append(node.children, child)
			}
		}
		return node
	}
	return extractHtmlNodes(doc), nil
}
