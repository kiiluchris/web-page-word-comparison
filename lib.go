package word_comparison

import (
	"errors"
	"sort"
	"strings"
)

type PageFetcher interface {
	FetchPage(path string) (string, error)
}

var ErrFileDoesNotExist = errors.New("Tried to fetch file but does not exist")
var ErrFileIsNotHtml = errors.New("File is not html")

func WordCounts(text string) WordCountMap {
	wc := make(map[string]uint)
	for _, word := range strings.Fields(text) {
		wc[word] += 1
	}
	return wc
}

type WordCount struct {
	Word  string `json:"text"`
	Count uint   `json:"count"`
}

type WordCountList []WordCount

func (l WordCountList) Len() int {
	return len(l)
}

func (l WordCountList) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

func (l WordCountList) Less(i, j int) bool {
	if l[i].Count == l[j].Count {
		return l[i].Word < l[j].Word
	} else {
		return l[i].Count > l[j].Count
	}
}

func (l WordCountList) Sort() {
	sort.Sort(l)
}

type WordCountMap map[string]uint

func (wcm WordCountMap) ToList() WordCountList {
	var wcl WordCountList = WordCountList{}

	for word, count := range wcm {
		wc := WordCount{
			Word:  word,
			Count: count,
		}
		wcl = append(wcl, wc)
	}
	wcl.Sort()

	return wcl
}

type WordCountApp struct {
	fetcher   PageFetcher
	parser    Parser
	fileStore FileStore
}

func NewWordCountApp(fetcher PageFetcher, parser Parser, fileStore FileStore) *WordCountApp {
	return &WordCountApp{
		fetcher:   fetcher,
		parser:    parser,
		fileStore: fileStore,
	}
}

type FetchedAndProcessedPage struct {
	FetchedPage   string
	ProcessedPage string
}

func (app *WordCountApp) FetchAndParse(path string) (*FetchedAndProcessedPage, error) {
	file, err := app.fetcher.FetchPage(path)
	if err != nil {
		return nil, err
	}
	parsedHtml, parseErr := app.parser.ParseText(file)
	if parseErr != nil {
		return nil, parseErr
	}
	data := FetchedAndProcessedPage{
		FetchedPage:   file,
		ProcessedPage: parsedHtml.ExtractText(),
	}

	return &data, nil
}

func (app *WordCountApp) Execute(path string) (WordCountMap, error) {
	wc := make(map[string]uint)
	data, err := app.FetchAndParse(path)
	if err != nil {
		return wc, err
	}
	return WordCounts(data.ProcessedPage), nil
}

type WordCountComparisonData struct {
	Common      map[int]WordCountList  `json:"common"`
	Unique      map[int]WordCountList  `json:"unique"`
	Occurrences []WordCountMapWithPage `json:"all_occurrences"`
}

func (self WordCountMap) Intersection(others ...WordCountMap) WordCountMap {
	result := WordCountMap{}

	for key, count := range self {
		isShared := true

		for _, other := range others {
			_, ok := other[key]
			if !ok {
				isShared = false
				break
			}
		}

		if isShared {
			result[key] = count
		}
	}

	return result
}

func (self WordCountMap) Difference(others ...WordCountMap) WordCountMap {
	result := WordCountMap{}

	for key, wc := range self {
		isShared := false

		for _, other := range others {
			_, ok := other[key]
			if ok {
				isShared = true
				break
			}
		}

		if !isShared {
			result[key] = wc
		}
	}

	return result
}

type WordCountMapWithPage struct {
	Page         Page         `json:"page"`
	WordCountMap WordCountMap `json:"word_occurrences"`
}

func MapWordCountMapWithPageToWordCountMap(wcms []WordCountMapWithPage) []WordCountMap {
	result := make([]WordCountMap, 0, len(wcms))
	for _, wcms := range wcms {
		result = append(result, wcms.WordCountMap)
	}
	return result
}

func filterIndex(wcl []WordCountMap, idx int) []WordCountMap {
	result := make([]WordCountMap, 0, len(wcl))
	for currentIndex, wcm := range wcl {
		if currentIndex != idx {
			result = append(result, wcm)
		}
	}
	return result
}

func CompareWordCountMaps(wordCountMapsWithPages []WordCountMapWithPage) WordCountComparisonData {
	if len(wordCountMapsWithPages) == 0 {
		return WordCountComparisonData{
			Occurrences: wordCountMapsWithPages,
		}
	}
	wordCountMaps := MapWordCountMapWithPageToWordCountMap(wordCountMapsWithPages)
	commonWordsFirst := wordCountMaps[0].Intersection(wordCountMaps[1:]...)
	commonWords := map[int]WordCountList{
		wordCountMapsWithPages[0].Page.Id: commonWordsFirst.ToList(),
	}
	for _, wordCountMapWithPage := range wordCountMapsWithPages[1:] {
		wordCountMap := WordCountMap{}
		for word := range commonWordsFirst {
			wordCountMap[word] = wordCountMapWithPage.WordCountMap[word]
		}
		commonWords[wordCountMapWithPage.Page.Id] = wordCountMap.ToList()
	}
	uniqueWords := make(map[int]WordCountList)
	for idx, wcm := range wordCountMapsWithPages {
		uniqueWords[wcm.Page.Id] = wcm.WordCountMap.Difference(filterIndex(wordCountMaps, idx)...).ToList()
	}

	return WordCountComparisonData{
		Occurrences: wordCountMapsWithPages,
		Common:      commonWords,
		Unique:      uniqueWords,
	}
}
