package word_comparison_test

import (
	"fmt"
	"gitlab.com/kiiluchris/web-page-word-comparison"
	"os"
	"reflect"
	"testing"

	"github.com/joho/godotenv"
)

const postgresTestDatabase = "test_pages"
const testFetchedFileDirectory = "test-data"
const testFileStorageDirectory = "test-data/files"

func TestPostgresPageStore(t *testing.T) {
	godotenv.Load("./.env")
	fetcher := word_comparison.NewFilePageFetcher(testFetchedFileDirectory)
	parser := word_comparison.NewHtmlParser(true, true)
	fileStore := word_comparison.NewFileSystemFileStore(testFileStorageDirectory)
	var store word_comparison.PageStore = word_comparison.NewPostgresPageStore(
    os.Getenv("POSTGRES_HOST"),
		os.Getenv("POSTGRES_DB"),
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		fileStore,
	)
	defer store.DeleteAllPages()
	page, _ := fetcher.FetchPage(validHtmlPath)
	htmlTree, _ := parser.ParseText(page)
	pageText := htmlTree.ExtractText()
	validHtmlPathUrl := fmt.Sprintf("localhost/%s", validHtmlPath)
	expectedNewPage := func(version int) word_comparison.Page {
		rawPath := word_comparison.PageUrlAsFilePath(validHtmlPathUrl)
		processedPath := fmt.Sprintf("%s.processed.v%d", rawPath, version)
		rawPath = fmt.Sprintf("%s.raw.v%d", rawPath, version)
		return word_comparison.Page{
			Id:          version,
			Url:         validHtmlPathUrl,
			RawTextPath: rawPath,
			TextPath:    processedPath,
			Version:     version,
		}
	}

	t.Run("page store url generation", func(t *testing.T) {
		exampleCom := "https://example.com"
		exampleCom2 := "https://example.com/"
		exampleComExpected := "example.com/_"
		assertStringEq(t, exampleComExpected, word_comparison.PageUrlAsFilePath(exampleCom))
		assertStringEq(t, exampleComExpected, word_comparison.PageUrlAsFilePath(exampleCom2))
		exampleComWithPath := "https://example.com/test"
		exampleComWithPath2 := "https://example.com/test/"
		exampleComWithPathExpected := "example.com/_test_"
		assertStringEq(t, exampleComWithPathExpected, word_comparison.PageUrlAsFilePath(exampleComWithPath))
		assertStringEq(t, exampleComWithPathExpected, word_comparison.PageUrlAsFilePath(exampleComWithPath2))
	})

	t.Run("list pages", func(t *testing.T) {
		pages, _ := store.GetPages()
		assertPageListEq(t, []word_comparison.Page{}, pages)
    pages, _ = store.GetPagesByIds([]int{1,2})
		assertPageListEq(t, []word_comparison.Page{}, pages)
	})

	t.Run("add new page", func(t *testing.T) {
		newPage, _ := store.AddPage(validHtmlPathUrl, page, pageText)
		assertPageEq(t, expectedNewPage(1), *newPage)
		newPage, _ = store.AddPage(validHtmlPathUrl, page, pageText)
		assertPageEq(t, expectedNewPage(2), *newPage)
		pages, _ := store.GetPages()
		assertPageListEq(t, []word_comparison.Page{expectedNewPage(1), expectedNewPage(2)}, pages)
    pages, _ = store.GetPagesByIds([]int{1,2})
		assertPageListEq(t, []word_comparison.Page{expectedNewPage(1), expectedNewPage(2)}, pages)
	})

	t.Run("list pages after adding new", func(t *testing.T) {
		pages, _ := store.GetPages()
		assertPageListEq(t, []word_comparison.Page{expectedNewPage(1), expectedNewPage(2)}, pages)
	})
	t.Run("get individual known pages", func(t *testing.T) {
		page1, _ := store.GetPage(1)
		assertPageNotNil(t, page1)
		assertPageEq(t, expectedNewPage(1), *page1)
		page2, _ := store.GetPage(2)
		assertPageNotNil(t, page2)
		assertPageEq(t, expectedNewPage(2), *page2)
	})
	t.Run("get page contents", func(t *testing.T) {
		got, err := store.GetPageProcessedTextById(1)
		assertErrorNil(t, err)
		if got != pageText {
			t.Fatalf("Invalid page text found: %q", got)
		}
	})
}

func assertStringEq(t testing.TB, expected, got string) {
	t.Helper()
	if expected != got {
		t.Fatalf("Expected %q, but got %q", expected, got)
	}
}

func assertPageEq(t testing.TB, expected, got word_comparison.Page) {
	t.Helper()
	if expected != got {
		t.Fatalf("Expected page to be %v, but got %v", expected, got)
	}
}

func assertPageNotNil(t testing.TB, expected *word_comparison.Page) {
	t.Helper()
	if expected == nil {
		t.Fatal("Expected page to not be nil, but got nil")
	}
}

func assertPageListEq(t testing.TB, expected, got []word_comparison.Page) {
	t.Helper()
	if !reflect.DeepEqual(expected, got) {
		t.Fatalf("Expected page to be %v, but got %v", expected, got)
	}
}
