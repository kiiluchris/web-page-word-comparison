package word_comparison_test

import (
	"gitlab.com/kiiluchris/web-page-word-comparison"
	"testing"
)

func assertErrorNil(t testing.TB, err error) {
	t.Helper()
	if err != nil {
		t.Fatalf("Expected error to be nil but got %q", err.Error())
	}
}

func assertError(t testing.TB, expected, err error) {
	t.Helper()
	if err == nil {
		t.Fatalf("Expected error to be %q but got nil", expected.Error())
	}
	if err != expected {
		t.Fatalf("Expected error to be %q but got %q", expected.Error(), err.Error())
	}
}

func assertMapKeyEq(t testing.TB, key string, expected uint, wc map[string]uint) {
	t.Helper()
	got, keyExists := wc[key]
	if !keyExists {
		t.Fatalf("Expected word count of key %q to be %d, but key does not exist in map", key, expected)
	}
	if got != expected {
		t.Fatalf("Expected word count of key %q to be %d, but found %d", key, expected, got)
	}
}

func assertWordCounts(t testing.TB, knownCounts []wordCount, wc map[string]uint) {
	t.Helper()
	for _, expected := range knownCounts {
		assertMapKeyEq(t, expected.word, expected.count, wc)
	}
}

type wordCount struct {
	word  string
	count uint
}

type wordCountList []wordCount

func (l wordCountList) AsMap() map[string]uint {
	m := make(map[string]uint)
	for _, c := range l {
		m[c.word] = c.count
	}
	return m
}

const validHtmlPath = "test.html"
const htmlAtPathThatDoesNotExist = "non-existent-file.html"
const nonHtmlPath = "test.md"

func TestFetchPage(t *testing.T) {
	fetcher := word_comparison.NewFilePageFetcher(testFetchedFileDirectory)
	fileStore := word_comparison.NewFileSystemFileStore(testFileStorageDirectory)
	parser := word_comparison.NewHtmlParser(true, true)
	knownCounts := wordCountList{
		{"cat", 2},
		{"dog", 2},
		{"log", 1},
		{"bat", 1},
		{"asd", 1},
	}
	app := word_comparison.NewWordCountApp(fetcher, parser, fileStore)
	t.Run("fetch html", func(t *testing.T) {
		wc, err := app.Execute(validHtmlPath)
		assertErrorNil(t, err)
		assertWordCounts(t, knownCounts, wc)
	})
	t.Run("fetch html at path that does not exist", func(t *testing.T) {
		_, err := app.Execute(htmlAtPathThatDoesNotExist)
		assertError(t, word_comparison.ErrFileDoesNotExist, err)
	})
	t.Run("fetch markdown", func(t *testing.T) {
		_, err := app.Execute(nonHtmlPath)
		assertError(t, word_comparison.ErrFileIsNotHtml, err)
	})
}

func TestCompareWordCountMaps(t *testing.T) {
	fetcher := word_comparison.NewFilePageFetcher(testFetchedFileDirectory)
	fileStore := word_comparison.NewFileSystemFileStore(testFileStorageDirectory)
	parser := word_comparison.NewHtmlParser(true, true)
	knownCounts := wordCountList{
		{"cat", 2},
		{"dog", 2},
		{"log", 1},
		{"bat", 1},
		{"asd", 1},
	}
	numberOfKnownCounts := len(knownCounts)
	app := word_comparison.NewWordCountApp(fetcher, parser, fileStore)
	t.Run("compare word counts empty list", func(t *testing.T) {
		expectedCommon := 0
		comparison := word_comparison.CompareWordCountMaps([]word_comparison.WordCountMapWithPage{})
		assertWordCountComparisonData(t, comparison, expectedCommon, []struct {
			key   int
			count int
		}{})
	})
	t.Run("compare word counts single element list", func(t *testing.T) {
		expectedCommon := 5
		wcm, err := app.Execute(validHtmlPath)
		assertErrorNil(t, err)
		wcmp := word_comparison.WordCountMapWithPage{
			Page:         word_comparison.Page{Id: 1},
			WordCountMap: wcm,
		}
		comparison := word_comparison.CompareWordCountMaps([]word_comparison.WordCountMapWithPage{wcmp})
		assertWordCountComparisonData(t, comparison, expectedCommon, []struct {
			key   int
			count int
		}{
			{1, expectedCommon},
		})
	})
	t.Run("compare word counts []{page, page}", func(t *testing.T) {
		wcm, err := app.Execute(validHtmlPath)
		assertErrorNil(t, err)
		wcmp := word_comparison.WordCountMapWithPage{
			Page:         word_comparison.Page{Id: 1},
			WordCountMap: wcm,
		}
		wcmp2 := word_comparison.WordCountMapWithPage{
			Page:         word_comparison.Page{Id: 2},
			WordCountMap: wcm,
		}
		comparison := word_comparison.CompareWordCountMaps([]word_comparison.WordCountMapWithPage{wcmp, wcmp2})
		assertWordCountComparisonData(t, comparison, numberOfKnownCounts, []struct {
			key   int
			count int
		}{
			{1, 0},
			{2, 0},
		})
	})
	t.Run("compare word counts []{page1, page2, page3}", func(t *testing.T) {
		wcmp1 := word_comparison.WordCountMapWithPage{
			Page: word_comparison.Page{Id: 1},
			WordCountMap: word_comparison.WordCountMap{
				"a":       1,
				"child":   2,
				"went":    3,
				"to":      5,
				"play":    1,
				"with":    3,
				"their":   4,
				"friends": 1,
			},
		}
		wcmp2 := word_comparison.WordCountMapWithPage{
			Page: word_comparison.Page{Id: 2},
			WordCountMap: word_comparison.WordCountMap{
				"a":       1,
				"child":   2,
				"went":    3,
				"to":      5,
				"eat":     1,
				"with":    3,
				"their":   4,
				"friends": 1,
			},
		}
		wcmp3 := word_comparison.WordCountMapWithPage{
			Page: word_comparison.Page{Id: 3},
			WordCountMap: word_comparison.WordCountMap{
				"a":        1,
				"child":    2,
				"went":     3,
				"for":      1,
				"swimming": 2,
				"lessons":  3,
			},
		}
		comparison := word_comparison.CompareWordCountMaps([]word_comparison.WordCountMapWithPage{wcmp1, wcmp2, wcmp3})
		expectedCommon := 3
		expectedUniqueLengths := []struct {
			key   int
			count int
		}{
			{1, 1},
			{2, 1},
			{3, 3},
		}
		assertWordCountComparisonData(t, comparison, expectedCommon, expectedUniqueLengths)
	})
}

func assertWordCountComparisonData(t testing.TB, comparison word_comparison.WordCountComparisonData, expectedCommonCount int, expectedUniqueLengths []struct {
	key   int
	count int
}) {
	t.Helper()
	foundCommonCount := len(comparison.Common)
	if foundCommonCount > 0 {
		foundCommonCount = len(comparison.Common[expectedUniqueLengths[0].key])
	}
	if expectedCommonCount != foundCommonCount {
		t.Fatalf("Expected %d common words but got %d", expectedCommonCount, foundCommonCount)
	}
	foundUniqueValues := len(comparison.Unique)
	expectedUniqueValues := len(expectedUniqueLengths)
	if expectedUniqueValues != foundUniqueValues {
		t.Fatalf("Expected %d unique keys but got %d: %v", expectedUniqueValues, foundUniqueValues, comparison.Unique)
	}
	for _, expected := range expectedUniqueLengths {
		numberUniqueValues := len(comparison.Unique[expected.key])
		if numberUniqueValues != expected.count {
			t.Fatalf("Expected %d unique values but got %d for page id %d", expected.count, numberUniqueValues, expected.key)
		}
	}
}
