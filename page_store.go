package word_comparison

import (
	"database/sql"
	"embed"
	"fmt"
	"io/fs"
	"net/http"
	"net/url"
	"path"
	"strings"

	"github.com/lib/pq"
	_ "github.com/lib/pq"
	"github.com/rubenv/sql-migrate"
)

//go:embed migrations/*
var migrationFs embed.FS

const migrationPath = "migrations"

type Page struct {
	Id          int    `json:"page_id"`
	Version     int    `json:"version"`
	Url         string `json:"url"`
	TextPath    string `json:"text"`
	RawTextPath string
}

type PageStore interface {
	GetPages() ([]Page, error)
  GetPagesByIds(pageIds []int) ([]Page, error)
	GetPage(pageId int) (*Page, error)
	GetPageProcessedTextById(id int) (string, error)
	GetPageProcessedText(pageUrl string) (string, error)
	AddPage(pageUrl, rawPage, pageText string) (*Page, error)
	DeleteAllPages()
}

type PostgresPageStore struct {
	db        *sql.DB
	fileStore FileStore
}

const DirectoryPerm = 0733
const FileUserRWPerm = 0644

func NewPostgresPageStore(host, database, user, password string, fileStore FileStore) *PostgresPageStore {
	dbinfo := fmt.Sprintf("host=%s user=%s password=%s dbname=%s", host, user, password, database)
	db, _ := sql.Open("postgres", dbinfo)
	if err := db.Ping(); err != nil {
		panic(err)
	}
	relativeFs, _ := fs.Sub(migrationFs, migrationPath)
	migrations := &migrate.HttpFileSystemMigrationSource{
		FileSystem: http.FS(relativeFs),
	}
	n, err := migrate.Exec(db, "postgres", migrations, migrate.Up)
	if err != nil {
		panic(err)
	}
	if n > 0 {
		fmt.Printf("Applied %d migrations!\n", n)
	}
	return &PostgresPageStore{
		db:        db,
		fileStore: fileStore,
	}
}

func (s *PostgresPageStore) GetPages() ([]Page, error) {
	rows, err := s.db.Query("SELECT * FROM pages")
	if err != nil {
		return []Page{}, err
	}
	var pages = []Page{}
	for rows.Next() {
		page := new(Page)
		err = rows.Scan(&page.Id, &page.Version, &page.Url, &page.TextPath, &page.RawTextPath)
		if err != nil {
			return pages, err
		}
		pages = append(pages, *page)
	}

	return pages, nil
}

func (s *PostgresPageStore) GetPagesByIds(pageIds []int) ([]Page, error) {
  if len(pageIds) == 0 {
    return []Page{}, nil
  }
  stmt, err := s.db.Prepare("SELECT * FROM pages WHERE pages.page_id = ANY($1)")
	rows, err := stmt.Query(pq.Array(pageIds))
	if err != nil {
		return []Page{}, err
	}
	var pages = []Page{}
	for rows.Next() {
		page := new(Page)
		err = rows.Scan(&page.Id, &page.Version, &page.Url, &page.TextPath, &page.RawTextPath)
		if err != nil {
			return pages, err
		}
		pages = append(pages, *page)
	}

	return pages, nil
}

func PageUrlObjAsFilePath(u *url.URL) string {
	urlPath := u.Path
	if urlPath == "" || urlPath[len(urlPath)-1:] != "/" {
		urlPath += "/"
	}
	return path.Join(u.Hostname(), strings.ReplaceAll(urlPath, "/", "_"))
}

func PageUrlAsFilePath(pageUrl string) string {
	u, _ := url.Parse(pageUrl)
	return PageUrlObjAsFilePath(u)
}

func (s *PostgresPageStore) AddPage(pageUrl, rawPage, pageText string) (*Page, error) {
	statement := `
  INSERT INTO pages (version, url, text_path, raw_text_path)
  SELECT
    COUNT(*) + 1,
    $1::VARCHAR,
    CONCAT($2::VARCHAR, '.v', COUNT(*) + 1),
    CONCAT($3::VARCHAR, '.v', COUNT(*) + 1)
  FROM pages
  WHERE pages.url = $1
  RETURNING page_id, version, text_path, raw_text_path`
	u, _ := url.Parse(pageUrl)
	pageRawPath := PageUrlObjAsFilePath(u)
	pageTextPath := pageRawPath + ".processed"
	pageRawPath = pageRawPath + ".raw"
	page := new(Page)
	page.Url = pageUrl
	row := s.db.QueryRow(statement, pageUrl, pageTextPath, pageRawPath)
	err := row.Scan(&page.Id, &page.Version, &page.TextPath, &page.RawTextPath)
	if err != nil {
		return nil, err
	}
	s.fileStore.SavePage(u.Hostname(), page.RawTextPath, rawPage, page.TextPath, pageText)

	return page, nil
}

func (s *PostgresPageStore) GetPage(id int) (*Page, error) {
	statement := `
  SELECT page_id, version, url, text_path, raw_text_path
  FROM pages WHERE page_id = $1`
	row := s.db.QueryRow(statement, id)
	page := new(Page)
	err := row.Scan(&page.Id, &page.Version, &page.Url, &page.TextPath, &page.RawTextPath)
	if err != nil {
		return nil, err
	}
	return page, nil
}

func (s *PostgresPageStore) DeleteAllPages() {
	s.db.Exec("DELETE FROM pages")
	s.db.Exec("ALTER SEQUENCE pages_page_id_seq RESTART WITH 1")
}

func (s *PostgresPageStore) GetPageProcessedTextById(id int) (string, error) {
	page, err := s.GetPage(id)
	if err != nil {
		return "", err
	}
	return s.GetPageProcessedText(page.TextPath)
}

func (s *PostgresPageStore) GetPageProcessedText(pageUrl string) (string, error) {
	return s.fileStore.GetFile(pageUrl)
}
